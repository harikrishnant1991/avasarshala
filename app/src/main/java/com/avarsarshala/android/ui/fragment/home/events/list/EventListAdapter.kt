package com.avarsarshala.android.ui.fragment.home.events.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.ItemEventBinding
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener
import com.avarsarshala.android.utils.getDateFromString
import com.avarsarshala.android.utils.getStringForDate

class EventListAdapter(private val listener: EventItemActionListener) : RecyclerView.Adapter<EventListAdapter.EventItemViewHolder>() {
    private val eventList = ArrayList<AvasarEvent>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ItemEventBinding>(layoutInflater, R.layout.item_event, parent, false)
        return EventItemViewHolder(
            binding,
            listener
        )
    }

    override fun getItemCount() = eventList.count()

    override fun onBindViewHolder(holder: EventItemViewHolder, position: Int) {
        holder.bind(eventList[position])
    }

    fun setEvents(itemList : ArrayList<AvasarEvent>) {
        this.eventList.clear()
        this.eventList.addAll(itemList)
        notifyDataSetChanged()
    }

    class EventItemViewHolder(private val binding: ItemEventBinding,
                              private val listener: EventItemActionListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(avasarEvent: AvasarEvent) {
            binding.category = avasarEvent.avasarType
            binding.title = avasarEvent.avasarName
            binding.type = avasarEvent.venueType
            val deadLine = getStringForDate(getDateFromString(avasarEvent.avasarDeadline))
            binding.deadLine = "Deadline: $deadLine"
            binding.event = avasarEvent
            binding.listener = listener
            binding.executePendingBindings()
        }
    }
}