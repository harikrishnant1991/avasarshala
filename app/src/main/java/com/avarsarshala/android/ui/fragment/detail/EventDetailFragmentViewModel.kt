package com.avarsarshala.android.ui.fragment.detail

import android.app.Application
import android.text.TextUtils
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.ui.base.BaseViewModel
import com.avarsarshala.android.utils.getDateFromString
import com.avarsarshala.android.utils.getStringForDate

class EventDetailFragmentViewModel(application: Application) : BaseViewModel(application) {

    var eventType = MutableLiveData<String>()
    var eventCategory = MutableLiveData<String>()
    var eventTitle = MutableLiveData<String>()
    var eventDeadLine = MutableLiveData<String>()
    var eventDate = MutableLiveData<String>()
    var registrationCost = MutableLiveData<String>()
    var benefits = MutableLiveData<String>()
    var eventVenue = MutableLiveData<String>()
    var isApplyVisible = MutableLiveData<Boolean>()

    var navigateUrl = MutableLiveData<String>()
    private var url = ""

    fun setEvent(event: AvasarEvent) {
        eventType.value = event.venueType
        eventCategory.value = event.avasarType
        eventTitle.value = event.avasarName
        if (!TextUtils.isEmpty(event.avasarDeadline)) {
            val deadline = getStringForDate(getDateFromString(event.avasarDeadline))
            eventDeadLine.value = deadline
        } else {
            eventDeadLine.value = "NA"
        }
        if (!TextUtils.isEmpty(event.avasarDday)) {
            val date = getStringForDate(getDateFromString(event.avasarDday))
            eventDate.value = date
        } else {
            eventDate.value = "NA"
        }
        if (!TextUtils.isEmpty(event.avasarDday)) {
            registrationCost.value = event.avasarCost
        } else {
            eventDate.value = "NA"
        }
        benefits.value = event.avasarBenefits
        eventVenue.value = event.avasarVenue
        url = event.avasarLink
        isApplyVisible.value = Patterns.WEB_URL.matcher(url).matches()
    }

    fun gotoApplyURL() {
        navigateUrl.value = url
    }
}