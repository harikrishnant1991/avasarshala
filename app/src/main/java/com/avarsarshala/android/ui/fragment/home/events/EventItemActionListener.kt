package com.avarsarshala.android.ui.fragment.home.events

import com.avarsarshala.android.network.api.event.AvasarEvent


interface EventItemActionListener {
    fun eventItemSelected(event: AvasarEvent)
}