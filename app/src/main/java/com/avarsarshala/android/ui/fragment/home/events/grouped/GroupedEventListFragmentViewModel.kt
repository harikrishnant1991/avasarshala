package com.avarsarshala.android.ui.fragment.home.events.grouped

import android.app.Application
import com.avarsarshala.android.model.event.GroupedEvents
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.ui.base.BaseViewModel
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener

class GroupedEventListFragmentViewModel(application: Application) : BaseViewModel(application),
    EventItemActionListener {

    private val eventListAdapter =
        GroupedEventListAdapter(getApplication(), this)

    lateinit var listener: EventItemActionListener

    fun getAdapter(): GroupedEventListAdapter {
        return eventListAdapter
    }

    fun setGroupedEvents(groupedEvents: ArrayList<GroupedEvents>) {
        eventListAdapter.setEvents(groupedEvents)
    }

    override fun eventItemSelected(event: AvasarEvent) {
        listener.eventItemSelected(event)
    }
}