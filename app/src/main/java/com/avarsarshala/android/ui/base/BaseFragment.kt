package com.avarsarshala.android.ui.base

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.avarsarshala.android.ui.dialog.ErrorDialog
import com.avarsarshala.android.ui.dialog.ErrorDialogListener
import com.avarsarshala.android.ui.dialog.LoaderDialog

open class BaseFragment : Fragment() {

    internal fun <T : BaseViewModel> attachCommonObserver(viewModel: T) {
        attachCommonObserver(viewModel, viewLifecycleOwner)
    }

    internal fun <T : BaseViewModel> attachCommonObserver(viewModel: T, owner: LifecycleOwner) {
        viewModel.isLoading.observe(owner, Observer { isLoading ->
            isLoading?.let { showLoadingDialog(it) }
        })
        viewModel.errorMessage.observe(owner, Observer {
            it?.let { errorMessage ->
                showErrorDialog(null, errorMessage)
                viewModel.errorMessage.value = null
            }
        })
    }

    private fun showLoadingDialog(show: Boolean) {
        try {
            if (show) {
                activity?.let {
                    LoaderDialog.show(it)
                }
            } else {
                LoaderDialog.dismiss()
            }
        } catch (ignored: Exception) {
        }
    }

    internal fun showErrorDialog(
        listener: ErrorDialogListener?,
        message: String,
        buttonName: String = "Ok"
    ) {
        try {
            ErrorDialog.show(activity!!, listener, message, buttonName)
        } catch (exception: Exception) { }
    }
}