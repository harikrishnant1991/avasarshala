package com.avarsarshala.android.ui.binding

import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import androidx.databinding.BindingAdapter


@BindingAdapter("bind:pager")
fun bindViewPagerTabs(view: TabLayout, pagerView: ViewPager) {
    view.setupWithViewPager(pagerView, true)
}