package com.avarsarshala.android.ui.fragment.home.events.list

import android.app.Application
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.ui.base.BaseViewModel
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener

class EventListFragmentViewModel(application: Application) : BaseViewModel(application),
    EventItemActionListener {

    private val eventListAdapter =
        EventListAdapter(this)

    lateinit var listener: EventItemActionListener

    fun getAdapter(): EventListAdapter {
        return eventListAdapter
    }

    fun setEvents(eventList: ArrayList<AvasarEvent>) {
        eventListAdapter.setEvents(eventList)
    }

    override fun eventItemSelected(event: AvasarEvent) {
        listener.eventItemSelected(event)
    }
}