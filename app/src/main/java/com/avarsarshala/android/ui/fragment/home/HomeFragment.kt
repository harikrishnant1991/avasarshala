package com.avarsarshala.android.ui.fragment.home


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentHomeBinding
import com.avarsarshala.android.ui.base.BaseFragment

class HomeFragment : BaseFragment() {
    private val viewModel by viewModels<HomeFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentHomeBinding>(inflater, R.layout.fragment_home, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.tabPagerAdapter = HomePagerAdapter(childFragmentManager)
        viewModel.setupTabs()
        attachFragmentObservers()
        binding.viewModel = viewModel
        return binding.root
    }

    private fun attachFragmentObservers() {
        attachCommonObserver(viewModel)
        viewModel.selectedEvent.observe(viewLifecycleOwner, Observer {
            it?.let { selectedEvent ->
                viewModel.selectedEvent.value = null
                val directions = HomeFragmentDirections.actionGlobalEventDetailFragment(selectedEvent)
                findNavController().navigate(directions)
            }
        })
    }
}
