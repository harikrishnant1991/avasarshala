package com.avarsarshala.android.ui.fragment.home.events.grouped


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentGroupedEventListBinding
import com.avarsarshala.android.model.event.GroupedEvents
import com.avarsarshala.android.ui.base.BaseFragment
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener

class GroupedEventListFragment(private val listener: EventItemActionListener) : BaseFragment() {
    private val viewModel by viewModels<GroupedEventListFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentGroupedEventListBinding>(inflater, R.layout.fragment_grouped_event_list, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.listener = listener
        binding.viewModel = viewModel
        return binding.root
    }

    fun setGroupedEvents(groupedEvents: ArrayList<GroupedEvents>) {
        viewModel.setGroupedEvents(groupedEvents)
    }
}
