package com.avarsarshala.android.ui.fragment.home.events.grouped

import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import com.avarsarshala.android.R
import com.avarsarshala.android.model.event.GroupedEvents
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener
import com.avarsarshala.android.utils.getDateFromString
import com.avarsarshala.android.utils.getStringForDate


class GroupedEventListAdapter(private val context: Context,
                              private val listener: EventItemActionListener) : BaseExpandableListAdapter() {
    private val groupedEvents = ArrayList<GroupedEvents>()

    override fun getGroupCount() = groupedEvents.count()

    override fun getGroup(groupPosition: Int) = groupedEvents[groupPosition]

    override fun getGroupId(groupPosition: Int) = 0L

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            val layoutInflater = context
                .getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = layoutInflater.inflate(R.layout.group_view_event, null)
        }
        val titleTV = view!!.findViewById<TextView>(R.id.groupTitleTV)
        titleTV.text = getGroup(groupPosition).eventType
        (parent as ExpandableListView).expandGroup(groupPosition)
        return view
    }

    override fun getChildrenCount(groupPosition: Int) = groupedEvents[groupPosition].events.count()

    override fun getChild(groupPosition: Int, childPosititon: Int)  = groupedEvents[groupPosition].events[childPosititon]

    override fun getChildId(groupPosition: Int, childPosititon: Int) = 0L

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (convertView == null) {
            val layoutInflater = context
                .getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = layoutInflater.inflate(R.layout.child_view_event, null)
        }
        val avasarEvent = getChild(groupPosition, childPosition)
        val titleTV = view!!.findViewById<TextView>(R.id.childTitleTV)
        val childTypeTV = view.findViewById<TextView>(R.id.childTypeTV)
        val childDeadlineTV = view.findViewById<TextView>(R.id.childDeadlineTV)
        titleTV.text = avasarEvent.avasarName
        childTypeTV.text = avasarEvent.venueType
        val deadLine = getStringForDate(getDateFromString(avasarEvent.avasarDeadline))
        childDeadlineTV.text = "Deadline: $deadLine"
        view.setOnClickListener {
            listener.eventItemSelected(avasarEvent)
        }
        return view
    }

    override fun isChildSelectable(groupPosition: Int, childPosititon: Int) = true

    override fun hasStableIds() = true

    fun setEvents(itemList : ArrayList<GroupedEvents>) {
        this.groupedEvents.clear()
        this.groupedEvents.addAll(itemList)
        notifyDataSetChanged()
    }
}