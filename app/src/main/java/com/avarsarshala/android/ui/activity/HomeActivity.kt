package com.avarsarshala.android.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem.SHOW_AS_ACTION_ALWAYS
import android.view.WindowManager
import com.avarsarshala.android.R
import com.avarsarshala.android.model.ModelManager

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
        setContentView(R.layout.activity_home)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuItem = menu?.add(ModelManager.userCategory)
        menuItem?.isCheckable = false
        menuItem?.isEnabled = false
        menuItem?.setShowAsAction(SHOW_AS_ACTION_ALWAYS)
        return true
    }
}
