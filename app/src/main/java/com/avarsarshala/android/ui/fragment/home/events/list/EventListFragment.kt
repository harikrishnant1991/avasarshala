package com.avarsarshala.android.ui.fragment.home.events.list


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentEventListBinding
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.ui.base.BaseFragment
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener

class EventListFragment(private val listener: EventItemActionListener) : BaseFragment() {
    private val viewModel by viewModels<EventListFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentEventListBinding>(inflater, R.layout.fragment_event_list, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.listener = listener
        binding.viewModel = viewModel
        return binding.root
    }

    fun setEvents(eventList: ArrayList<AvasarEvent>) {
        viewModel.setEvents(eventList)
    }
}
