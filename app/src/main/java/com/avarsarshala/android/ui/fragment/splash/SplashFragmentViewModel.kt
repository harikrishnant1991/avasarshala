package com.avarsarshala.android.ui.fragment.splash

import android.app.Application
import com.avarsarshala.android.ui.base.BaseViewModel

class SplashFragmentViewModel(application: Application) : BaseViewModel(application)