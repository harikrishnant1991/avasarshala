package com.avarsarshala.android.ui.dialog

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import androidx.databinding.DataBindingUtil
import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.DialogErrorBinding

object ErrorDialog {
    private var dialog: Dialog? = null
    private var listener: ErrorDialogListener? = null

    fun show(
        context: Context,
        listener: ErrorDialogListener?,
        message: String,
        buttonName: String
    ) {
        ErrorDialog.listener = listener
        if (dialog == null) {
            dialog = Dialog(context, R.style.ErrorDialogTheme)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val binding = DataBindingUtil.inflate<DialogErrorBinding>(
                LayoutInflater.from(context),
                R.layout.dialog_error,
                null,
                false)
            dialog?.setContentView(binding.root)
            dialog?.setCancelable(false)
            binding.message = message
            binding.buttonTitle = buttonName
            binding.setListener {
                dismiss()
            }
        }
        else if (dialog?.isShowing == true) {
            return
        }
        try {
            dialog?.show()
        } catch (exception: Exception) {
        }
    }

    fun dismiss() {
        if (dialog != null && dialog?.isShowing == true) {
            dialog?.dismiss()
            dialog = null
            listener?.errorDialogDismissed()
        }
    }
}

interface ErrorDialogListener {
    fun errorDialogDismissed()
}