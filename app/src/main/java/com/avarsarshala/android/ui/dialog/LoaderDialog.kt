package com.avarsarshala.android.ui.dialog

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.Window.FEATURE_NO_TITLE
import com.avarsarshala.android.R
import java.lang.Exception


object LoaderDialog {

    private var dialog: Dialog? = null

    fun show(context: Context) {
        if (dialog == null) {
            dialog = Dialog(context, R.style.LoaderDialogStyle)
            dialog?.requestWindowFeature(FEATURE_NO_TITLE)
            dialog?.setContentView(R.layout.layout_loading_indicator)
            dialog?.setCancelable(false)
        }
        else if (dialog?.isShowing == true) {
            return
        }
        try {
            dialog?.show()
        } catch (ignored: Exception) {

            Log.d("Loader_Show", "Loader is shown")
        }
    }

    fun dismiss() {
        if (dialog != null && dialog?.isShowing == true) {
            dialog?.dismiss()
        }
        dialog = null
    }
}