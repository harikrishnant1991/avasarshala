package com.avarsarshala.android.ui.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    internal var isLoading = MutableLiveData<Boolean>()
    internal var errorMessage = MutableLiveData<String>()
}