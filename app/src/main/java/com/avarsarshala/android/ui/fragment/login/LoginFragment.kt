package com.avarsarshala.android.ui.fragment.login


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentLoginBinding
import com.avarsarshala.android.ui.activity.HomeActivity
import com.avarsarshala.android.ui.base.BaseFragment

class LoginFragment : BaseFragment() {

    private val viewModel by viewModels<LoginFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentLoginBinding>(inflater, R.layout.fragment_login, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        attachFragmentObservers()
        return binding.root
    }

    private fun attachFragmentObservers() {
        attachCommonObserver(viewModel)
        viewModel.isUserLoggedIn.observe(viewLifecycleOwner, Observer {
            it?.let { isUserLoggedIn ->
                if (isUserLoggedIn) {
                    viewModel.isUserLoggedIn.value = null
                    val intent = Intent(activity, HomeActivity::class.java)
                    activity?.startActivity(intent)
                    activity?.finish()
                }
            }
        })
    }
}
