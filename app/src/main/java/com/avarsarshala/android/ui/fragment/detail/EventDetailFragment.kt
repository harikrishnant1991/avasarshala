package com.avarsarshala.android.ui.fragment.detail


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentEventDetailBinding
import com.avarsarshala.android.ui.base.BaseFragment
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.viewModels


class EventDetailFragment : BaseFragment() {

    private val args by navArgs<EventDetailFragmentArgs>()
    private val viewModel by viewModels<EventDetailFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentEventDetailBinding>(inflater, R.layout.fragment_event_detail, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        viewModel.setEvent(args.avarsarEvent)
        binding.viewModel = viewModel
        attachFragmentObservers()
        return binding.root
    }

    private fun attachFragmentObservers() {
        attachCommonObserver(viewModel)
        viewModel.navigateUrl.observe(viewLifecycleOwner, Observer {
            it?.let { navigateUrl ->
                viewModel.navigateUrl.value = null
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(navigateUrl))
                startActivity(browserIntent)
            }
        })
    }
}
