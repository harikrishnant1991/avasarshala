package com.avarsarshala.android.ui.fragment.home

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.avarsarshala.android.model.ModelManager
import com.avarsarshala.android.model.event.GroupedEvents
import com.avarsarshala.android.network.api.event.AvasarEvent
import com.avarsarshala.android.network.api.event.EventListAPIListener
import com.avarsarshala.android.network.api.event.EventListAPIManager
import com.avarsarshala.android.ui.base.BaseViewModel
import com.avarsarshala.android.ui.fragment.home.events.EventItemActionListener
import com.avarsarshala.android.ui.fragment.home.events.grouped.GroupedEventListFragment
import com.avarsarshala.android.ui.fragment.home.events.list.EventListFragment
import com.avarsarshala.android.utils.getDateFromString
import com.avarsarshala.android.utils.getUserTypeValue
import java.util.*
import kotlin.collections.ArrayList

class HomeFragmentViewModel(application: Application) : BaseViewModel(application),
    EventListAPIListener, EventItemActionListener {

    internal lateinit var tabPagerAdapter: HomePagerAdapter

    private val allEventsFragment =
        GroupedEventListFragment(this)
    private val thisWeekEventsFragment =
        EventListFragment(this)

    var selectedEvent = MutableLiveData<AvasarEvent>()

    init {
        getEventList()
    }

    private fun getEventList() {
        isLoading.value = true
        EventListAPIManager(this).getEventList()
    }

    fun getAdapter(): HomePagerAdapter? {
        return tabPagerAdapter
    }

    fun setupTabs() {
        tabPagerAdapter.addFragment(thisWeekEventsFragment, "THIS WEEK")
        tabPagerAdapter.addFragment(allEventsFragment, "ALL EVENTS")
    }

    override fun eventListFetched(eventList: ArrayList<AvasarEvent>) {
        allEventsFragment.setGroupedEvents(getUGEvents(eventList))
        thisWeekEventsFragment.setEvents(getThisWeekEvents(eventList))
        isLoading.value = false
    }

    override fun eventListFetchFailed(message: String) {
        isLoading.value = false
        errorMessage.value = message
    }

    private fun getUGEvents(eventList: ArrayList<AvasarEvent>) : ArrayList<GroupedEvents> {
        val allFutureEvents = ArrayList(eventList.filter { event ->
            val date = getDateFromString(event.avasarDeadline)
            val today = Date()
            val difference = date.time - today.time
            val daysBetween = (difference.toDouble() / (1000 * 60 * 60 * 24).toDouble())
            val shouldFilter = daysBetween > -1
            shouldFilter
                    && getUserTypeValue(event) == "Y"
                    && (event.avasarDistrict.toLowerCase() == ModelManager.userDistrict.toLowerCase()
                    || event.avasarDistrict.toLowerCase() == "online")
        })
        val grouped = allFutureEvents.groupBy { event ->
            event.avasarType
        }
        val groupedEvents = ArrayList<GroupedEvents>()
        val sortedKeys = grouped.keys.sorted()
        for (key in sortedKeys) {
            val sortedValues = grouped.getValue(key).sortedBy { event ->
                val date = getDateFromString(event.avasarDeadline)
                date
            }
            groupedEvents.add(GroupedEvents(key, ArrayList(sortedValues)))
        }
        return groupedEvents
    }

    private fun getThisWeekEvents(eventList: ArrayList<AvasarEvent>) : ArrayList<AvasarEvent> {
        val thisWeekEvents  = ArrayList(eventList.filter { event ->
            val date = getDateFromString(event.avasarDeadline)
            val today = Date()
            val difference = date.time - today.time
            val daysBetween = (difference.toDouble() / (1000 * 60 * 60 * 24).toDouble())
            val shouldFilter = daysBetween in -1.0..7.0
            shouldFilter
                    && getUserTypeValue(event) == "Y"
                    && (event.avasarDistrict.toLowerCase() == ModelManager.userDistrict.toLowerCase()
                    || event.avasarDistrict.toLowerCase() == "online")
        })
        val grouped = thisWeekEvents.groupBy { event ->
            event.avasarType
        }
        thisWeekEvents.clear()
        for (events in grouped.values) {
            val sorted = events.sortedBy { event ->
                val date = getDateFromString(event.avasarDeadline)
                date
            }
            thisWeekEvents.addAll(sorted)
        }
        return thisWeekEvents
    }

    override fun eventItemSelected(event: AvasarEvent) {
        selectedEvent.value = event
    }
}