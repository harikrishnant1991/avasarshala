package com.avarsarshala.android.ui.fragment.splash


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController

import com.avarsarshala.android.R
import com.avarsarshala.android.databinding.FragmentSplashBinding
import com.avarsarshala.android.model.ModelManager
import com.avarsarshala.android.ui.activity.HomeActivity
import com.avarsarshala.android.ui.base.BaseFragment

class SplashFragment : BaseFragment() {
    private val viewModel by viewModels<SplashFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = DataBindingUtil.inflate<FragmentSplashBinding>(inflater, R.layout.fragment_splash, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleLogin()
    }

    private fun handleLogin() {
        if (ModelManager.isUserLoggedIn) {
            val intent = Intent(activity, HomeActivity::class.java)
            activity?.startActivity(intent)
            activity?.finish()
        } else {
            val directions = SplashFragmentDirections.actionSplashFragmentToLoginFragment()
            findNavController().navigate(directions)
        }
    }
}
