package com.avarsarshala.android.ui.fragment.login

import android.app.Application
import android.util.Patterns
import androidx.lifecycle.MutableLiveData
import com.avarsarshala.android.model.ModelManager
import com.avarsarshala.android.network.api.login.User
import com.avarsarshala.android.network.api.login.UserListAPIListener
import com.avarsarshala.android.network.api.login.UserListAPIManager
import com.avarsarshala.android.ui.base.BaseViewModel

class LoginFragmentViewModel(application: Application) : BaseViewModel(application),
    UserListAPIListener {

    var emailID = MutableLiveData<String>()
    var password = MutableLiveData<String>()

    var isUserLoggedIn = MutableLiveData<Boolean>()

    fun login() {
        if (emailID.value == null || emailID.value!! == "" || !Patterns.EMAIL_ADDRESS.matcher(emailID.value!!).matches()) {
            errorMessage.value = "Invalid Email Address"
        }
        if (password.value == null || password.value!! == "" ) {
            errorMessage.value = "Invalid Password"
        } else {
            isLoading.value = true
            UserListAPIManager(this).getUserList()
        }
    }

    override fun userListFetched(userList: ArrayList<User>) {
        isLoading.value = false
        val user = userList.firstOrNull { user ->
            user.email == emailID.value?:"" && user.password == password.value?:""
        }
        user?.let {
            ModelManager.isUserLoggedIn = true
            ModelManager.userCategory = it.category
            ModelManager.userDistrict = it.district
            ModelManager.saveLoginDetails(getApplication())
            isUserLoggedIn.value = true
        }
    }

    override fun userListFetchFailed(message: String) {
        isLoading.value = false
        errorMessage.value = message
    }
}