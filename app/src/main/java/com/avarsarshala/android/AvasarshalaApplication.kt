package com.avarsarshala.android

import android.app.Application
import com.avarsarshala.android.model.ModelManager

class AvasarshalaApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        ModelManager.loadLoginDetails(this)
    }
}