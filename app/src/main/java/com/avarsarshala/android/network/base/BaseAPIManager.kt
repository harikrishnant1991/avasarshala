package com.avarsarshala.android.network.base

import com.avarsarshala.android.network.model.CommunicationModelManager
import retrofit2.Retrofit

open class BaseAPIManager {
    protected val retrofit: Retrofit = CommunicationModelManager.retrofit
}