package com.avarsarshala.android.network.api.login

class User(internal val email: String,
           internal val password: String,
           internal val category: String,
           internal val district: String)