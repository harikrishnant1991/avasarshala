package com.avarsarshala.android.network.api.event

interface EventListAPIListener {
    fun eventListFetched(eventList: ArrayList<AvasarEvent>)
    fun eventListFetchFailed(message: String)
}