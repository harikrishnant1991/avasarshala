package com.avarsarshala.android.network.api.login

import com.avarsarshala.android.network.base.BaseAPIManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UserListAPIManager(internal val listener: UserListAPIListener) : BaseAPIManager(), Callback<ArrayList<User>> {

    fun getUserList() {
        val api = retrofit.create(UserListAPI::class.java)
        val apiCall = api.getEventsList()
        apiCall.enqueue(this)
    }

    override fun onResponse(call: Call<ArrayList<User>>, response: Response<ArrayList<User>>) {
        if (response.body() != null) {
            listener.userListFetched(response.body()!!)
        } else {
            listener.userListFetchFailed("Authentication Failure")
        }
    }

    override fun onFailure(call: Call<ArrayList<User>>, t: Throwable) {
        listener.userListFetchFailed("Authentication Failure")
    }
}