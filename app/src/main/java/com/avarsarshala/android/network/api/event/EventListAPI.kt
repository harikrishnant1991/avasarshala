package com.avarsarshala.android.network.api.event

import retrofit2.Call
import retrofit2.http.GET

interface EventListAPI {
    @GET("events.json")
    fun getEventsList() : Call<ArrayList<AvasarEvent>>
}