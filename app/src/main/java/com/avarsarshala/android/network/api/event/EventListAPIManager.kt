package com.avarsarshala.android.network.api.event

import com.avarsarshala.android.network.base.BaseAPIManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EventListAPIManager(internal val listener: EventListAPIListener) : BaseAPIManager(), Callback<ArrayList<AvasarEvent>> {

    fun getEventList() {
        val api = retrofit.create(EventListAPI::class.java)
        val apiCall = api.getEventsList()
        apiCall.enqueue(this)
    }

    override fun onResponse(
        call: Call<ArrayList<AvasarEvent>>,
        response: Response<ArrayList<AvasarEvent>>
    ) {
        if (response.body() != null) {
            listener.eventListFetched(response.body()!!)
        } else {
            listener.eventListFetchFailed("Unable to fetch event list")
        }
    }

    override fun onFailure(call: Call<ArrayList<AvasarEvent>>, t: Throwable) {
        listener.eventListFetchFailed("Unable to fetch event list")
    }
}