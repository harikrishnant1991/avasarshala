package com.avarsarshala.android.network.api.login

interface UserListAPIListener {
    fun userListFetched(userList: ArrayList<User>)
    fun userListFetchFailed(message: String)
}