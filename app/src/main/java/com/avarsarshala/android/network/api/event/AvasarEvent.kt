package com.avarsarshala.android.network.api.event

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AvasarEvent(
    internal var avasarName: String,
    internal var avasarType: String,
    internal var avasarDeadline: String,
    internal var avasarDday: String,
    internal var avasarCost: String,
    internal var avasarBenefits: String,
    internal var avasarLink: String,
    internal var avasarVenue: String,
    internal var avasarDistrict: String,
    internal var venueType: String,
    internal var lkg: String,
    internal var ukg: String,
    internal var cl1: String,
    internal var cl2: String,
    internal var cl3: String,
    internal var cl4: String,
    internal var cl5: String,
    internal var cl6: String,
    internal var cl7: String,
    internal var cl8: String,
    internal var cl9: String,
    internal var cl10: String,
    internal var cl11: String,
    internal var cl12: String,
    internal var ug: String,
    internal var pg: String,
    internal var y: String
) : Parcelable