package com.avarsarshala.android.network.api.login

import retrofit2.Call
import retrofit2.http.GET

interface UserListAPI {
    @GET("login.json")
    fun getEventsList() : Call<ArrayList<User>>
}