package com.avarsarshala.android.utils

import com.avarsarshala.android.model.ModelManager
import com.avarsarshala.android.network.api.event.AvasarEvent
import java.text.SimpleDateFormat
import java.util.*

fun getDateFromString(dateString: String) : Date {
    var newDateString = dateString.replace("T", " ")
    newDateString = newDateString.replace("Z", "")
    val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH)
    return dateFormat.parse(newDateString)!!
}

fun getStringForDate(date: Date) : String {
    val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
    return dateFormat.format(date)
}

fun getUserTypeValue(event: AvasarEvent): String {
    return when (ModelManager.userCategory.toLowerCase()) {
        "lkg" -> event.lkg
        "ukg" -> event.ukg
        "cl1" -> event.cl1
        "cl2" -> event.cl2
        "cl3" -> event.cl3
        "cl4" -> event.cl4
        "cl5" -> event.cl5
        "cl6" -> event.cl6
        "cl7" -> event.cl7
        "cl8" -> event.cl8
        "cl9" -> event.cl9
        "cl10" -> event.cl10
        "cl11" -> event.cl11
        "cl12" -> event.cl12
        "ug" -> event.ug
        "pg" -> event.pg
        "y" -> event.y
        else -> ""
    }
}