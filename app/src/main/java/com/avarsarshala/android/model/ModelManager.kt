package com.avarsarshala.android.model

import android.content.Context
import androidx.core.content.edit

object ModelManager {
    private const val SHARED_PREFERENCE_KEY = "AVASARASHALA_SHARED_PREFERENCES"
    private const val SHARED_PREFERENCE_IS_USER_LOGGED_IN_KEY = "IS_USER_LOGGED_IN"
    private const val SHARED_PREFERENCE_USER_CATEGORY_KEY = "USER_CATEGORY"
    private const val SHARED_PREFERENCE_USER_DISTRICT_KEY = "USER_DISTRICT"

    internal var isUserLoggedIn = false
        set(value) {
            if (!value) {
                userCategory = ""
                userDistrict = ""
            }
            field = value
        }

    internal var userCategory = ""
    internal var userDistrict = ""

    internal fun saveLoginDetails(context: Context) {
        val sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        sharedPreferences.edit {
            putBoolean(
                SHARED_PREFERENCE_IS_USER_LOGGED_IN_KEY,
                isUserLoggedIn
            )
            if (isUserLoggedIn) {
                putString(SHARED_PREFERENCE_USER_CATEGORY_KEY, userCategory)
                putString(SHARED_PREFERENCE_USER_DISTRICT_KEY, userDistrict)
            }
        }
    }

    internal fun clearLoginDetails(context: Context) {
        isUserLoggedIn = false
        val sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        sharedPreferences.edit {
            clear()
        }
    }

    internal fun loadLoginDetails(context: Context) {
        val sharedPreferences =
            context.getSharedPreferences(SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE)
        isUserLoggedIn = sharedPreferences.getBoolean(
            SHARED_PREFERENCE_IS_USER_LOGGED_IN_KEY, false
        )
        userCategory = sharedPreferences.getString(SHARED_PREFERENCE_USER_CATEGORY_KEY, "")!!
        userDistrict = sharedPreferences.getString(SHARED_PREFERENCE_USER_DISTRICT_KEY, "")!!
    }
}