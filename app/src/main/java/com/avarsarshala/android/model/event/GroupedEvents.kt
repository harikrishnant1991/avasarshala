package com.avarsarshala.android.model.event

import com.avarsarshala.android.network.api.event.AvasarEvent

data class GroupedEvents(internal val eventType: String,
                         internal val events: ArrayList<AvasarEvent>)